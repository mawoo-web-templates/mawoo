const state = {
  screen: {
    type: ""
  }
};

const getters = {
  getType: state => {
    return state.screen.type;
  }
};

const mutations = {
  changeType: (state, screenType) => {
    state.screen.type = screenType;
  }
};

const actions = {
  changeTypeCommit: ({ commit }, screenType) => {
    commit("changeType", screenType);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
